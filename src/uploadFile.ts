import { GaxiosPromise } from "@googleapis/drive";
import { createReadStream, existsSync } from "fs";
import { drive_v3 } from "googleapis";
import { lookup } from "mime-types";
import { basename, join } from "path";
import { googleDrive } from "./googleDrive";
import { targetFolderInGdrive } from "./paths";

export const uploadFile = (
  path: string
): GaxiosPromise<drive_v3.Schema$File> => {
  const mimeType = lookup(path);

  if (!mimeType) {
    return Promise.reject(`Could not determine mime type from ${path}`);
  }

  if (!existsSync(path)) {
    return Promise.reject(`File ${path} to upload does not exist`);
  }

  return googleDrive.files.create({
    requestBody: {
      name: join(targetFolderInGdrive, basename(path)),
      mimeType: mimeType,
    },
    media: {
      mimeType: mimeType,
      body: createReadStream(path),
    },
  });
};

import { getEnvValueOrThrow } from "@nvon/baseline";

export const targetFolderInGdrive = getEnvValueOrThrow(
  "TARGET_FOLDER_IN_GDRIVE",
  process.env
);

export const folderToBackup = getEnvValueOrThrow(
  "FOLDER_TO_BACKUP",
  process.env
);

import { getEnvValueOrThrow } from "@nvon/baseline";
import { config as dotenvSafe } from "dotenv-safe";
import { google } from "googleapis";

dotenvSafe();

const oauth2Client = new google.auth.OAuth2(
  getEnvValueOrThrow("GOOGLE_CLIENT_ID", process.env),
  getEnvValueOrThrow("GOOGLE_CLIENT_SECRET", process.env),
  getEnvValueOrThrow("GOOGLE_REDIRECT_URL", process.env)
);

export const googleDrive = google.drive({
  version: "v3",
  auth: oauth2Client,
});

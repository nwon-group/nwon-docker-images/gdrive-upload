import { readdirSync } from "fs";
import { uploadFile } from "./uploadFile";

/* 
Copies all files in folder to GDrive
*/
export const backupFolderToGDrive = (folder: string) => {
  return Promise.all(readdirSync(folder).map((file) => uploadFile(file)));
};
